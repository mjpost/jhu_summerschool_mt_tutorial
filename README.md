# Rule-based Machine Translation

Tutorial developed with Kevin Duh, Rebecca Knowles, and Huda Khayrallah for the
2016 JHU CLSP summer school.

  http://www.clsp.jhu.edu/workshops/16-workshop/jhu-summer-school-on-human-language-technology/
  
For installation instructions, please see: SETUP.md

The goal is to provide something in the spirit of Kevin Knight's alignment tutorial, but updated to give students a fuller feel of both the history and challenges of MT. 

The idea is to start the first day, not with a lecture, but with a lab. In this lab, the students are given a web interface to a French-English MT system that has no phrase table. They are then instructed to write the rules for the translation system, which can be easily added via a web-interface. These then take effect immediately. At any time, they can translate the test document and see its output. The idea is that they will write and edit rules over the course of 90-minute lab to see how good they can make the MT output. 

As an additional complication, when they add rules, they can also add arbitrary binary features, and then adjust the weights, to see how that affects the output. For example, they could create rules with a feature "priority=1", and then they could jack up the weight on the "priority" feature so the decoder will prefer those rules.

(In an alternate version we are considering, the source language is made-up. They students are given a small bitext between this made-up language and some foreign language, say French; they then have to use this to come up with the rules. But at the moment I think the task will be hard enough even knowing the foreign language).

We can also group the students into teams, and have them compete to see who can do the best translating the document. They could also choose their source language, if we used some n-way parallel document like the Bible or the UN Declaration on Human Rights.

After 90 minutes, the students should:

- Have an appreciation for the difficulties of developing rule-based systems, which will in effect have taken them through the history of rule-based approaches to MT

- Be asking questions, such as 
	- How can we automate the learning of these rules?
	- How can they be assigned weights?
	- How can we make the target output more fluent
	- How can we tell if we're actually producing better results?

That sets them up for an afternoon lecture introducing them to statistical MT, and its answers to these questions.
