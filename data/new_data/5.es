Mire V. esta flor blanca.
¿Sabes qué flor es?
Es la flor de la yuca.
Nos gusta verla brillar en el sol.
Les gusta a las abejas y a las polillas.
Ellas cogen comida de sus capullos.
La planta de la yuca tiene hojas largas y puntiagudas.
Es una planta muy útil.
Catalina tiene una soga para su cabra.
La soga está hecha de las hojas y de los tallos de la yuca.
Su madre halla útiles las raíces.
Las arranca y las seca.
Después las usa para jabón.
Lava el cabello de Catalina con ellas.
Catalina tiene un hermoso cabello negro.
La yuca lo pone suave y lustroso.
El fruto de la yuca es bueno para comer.
La madre de Catalina cuece el fruto.
"¡Qué hermosa es la yuca!" dicen los niños.
"¡Qué útil es!" dice su madre.
"Nos alegra a todos ver la planta de la yuca."