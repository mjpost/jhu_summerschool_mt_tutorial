Una abeja sale de un huevo.
Primeramente es un gusano pequeño.
Las abejas lo alimentan de polen.
Recogen el polen de las flores.
El polen parece polvo amarillo.
El gusano se alimenta durante cinco días, entonces parece que va a dormir.
Las abejas lo cubren con cera.
Al poco tiempo se despierta y sale de su cama de cera.
Es una abeja chiquitina.