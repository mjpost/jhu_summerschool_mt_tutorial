#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
reload(sys)
sys.setdefaultencoding("utf-8")

from nltk.tokenize import wordpunct_tokenize as wt
def tokenize(line):
    return wt(line)

#Replace special characters.
def rep_chars(w):
    umlautDictionary = {u'Ä': 'Ae',
                        u'Ö': 'Oe',
                        u'Ü': 'Ue',
                        u'ä': 'ae',
                        u'ö': 'oe',
                        u'ü': 'ue',
                        u'ü': 'ue',
                        u'ß': 'ss',
                        u'»': '"',
                        u'«': '"',
                        u'é': 'ee',
                        u'í': 'ii',
                        u'ú': 'uu',
                        u'¡': '',
                        u'á': 'aa',
                        u'¿': '',
                        u'É': 'Ee',
                        u'ñ': 'ny',
                        u'ó': 'oo'
                        }
    for char in umlautDictionary:
        if char in w:
            w = w.replace(char, umlautDictionary[char])

    return w

#Cipher.
lower = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
shuff = ['o', 'x', 'g', 'p', 'a', 'm', 'f', 'z', 'e', 'h', 'c', 'v', 'j', 'b', 'i', 'n', 't', 's', 'd', 'r', 'w', 'q', 'y', 'k', 'u', 'l']
upper = [x.upper() for x in lower]
ushuff = [x.upper() for x in shuff]

cipherDictionary = dict(zip(lower+upper, shuff+ushuff))

#Write out ciphertext.
def cipher(fn):
    with open(fn) as f:
        for line in f:
            new_line = []
            for w in tokenize(rep_chars(line.decode('utf-8'))):
                temp_word = ''
                for c in w:
                    temp_word += cipherDictionary[c] if c in cipherDictionary else c
                new_line.append(temp_word)
            print " ".join(new_line)
                

if __name__=="__main__":
    cipher(sys.argv[1])
