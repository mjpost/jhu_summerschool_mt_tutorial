Un ratoncito vivía en nuestras paredes.
Todas las noches salía para jugar.
Venía al cuarto de Sofía.
Le gusta jugar sobre su escritorio.
Sofía ponía a veces azúcar allí para él.
El ratoncito la encontraba.
Un día mamá lo vio.
Dijo que el gatito debía cogerlo.
Sofía estaba muy triste.
No quería que se lo comiese el gatito.
Habló a papá del ratoncito.
Él le dio una ratonera pequeñita que parecía una jaula.
El ratoncito podía vivir en ella.
Puso azúcar en la ratonera.
El ratoncito entró en la ratonera.
Ella le daba de comer todos los días.
Ella le daba agua en la taza de su muñeca.
El ratoncito quiere a Sofía.
Está feliz en su jaula.