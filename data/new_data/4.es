Yo soy un mayito.
Hago mi nido en los prados.
Mira mi hermoso plumaje.
Es blanco y negro.
¿Ve V. a mi compañerita?
Tiene un plumaje oscuro.
A mí me gustan las margaritas y las yerbas.
Me balanceo en los jazmines y en las zarzas.
Soy tan feliz y tan alegre.
Vuelo hacia los campos de arroz.
Como todo el arroz que puedo.
Yo creo que el arroz crece para mí.
Cojo insectos, moscas y gusanos.
Y creo que yo podría tener arroz también.
En el verano voy al norte.
¡Mayito, mayito! ésta es mi canción.
Búscame en los prados.