¡Mire V. las abejas!
Mire V. cómo vuelan a sus colmenas.
Recogen la miel de las flores y la ponen en sus colmenas.
A María le gusta mirar las abejas.
Le gusta verlas recoger la miel.
No la pican.
A ella le gusta ayudarlas.
María coge una bonita flor amarilla y se la trae a una abeja.
La abeja vuela hacia la flor.
No la pica.
María observa la abeja recogiendo miel.
Quiere ver cómo lo hace.
Éstas son abejas que hacen miel.