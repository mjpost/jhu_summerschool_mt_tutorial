El mayito no tiene un pico muy puntiaguda.
La cabra de la madre de María come las yerbas y los jazmines y es muy feliz.
¿Ve V. a mi cabra blanca?
Creo que el arroz no crece en los prados.
Catalina y su madre recogen el fruto de la yuca en el jardín y en los prados.
El ratoncito pequeño come el azúcar en su jaula en el cuarto de Sofía.
Catalina puso agua en la taza de su muñeca para su ratoncito feliz.
La abeja no la pica.
A la abeja le gusta la flor amarilla.
María quiere ver cinco abejas y un gusano.
¡Mire V. la abeja chiquitina en su cama de cera en las colmenas!
La madre de Catalina quiere a Catalina.
Sofía cuece el arroz con agua y ella lo come.
El mayito vuela hacia los prados y las colmenas de las abejas.
Un gatito es un gato muy pequeño.
Todos los días Sofía come arroz con azúcar mientras mira a las abejas amarillas.
