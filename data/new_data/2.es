María jugaba en el jardín un día.
Oyó un zumbido fuerte.
Era demasiado fuerte para una abeja.
Era un colibrí.
María se quedó quieta para mirar el colibrí.
¡Qué bonitas eran sus plumas!
¡Qué aprisa movía las alitas!
Tenía un pico muy largo.
Podía llegar con él al fondo de las flores.
El colibrí come miel.
La recoge de las flores.
Es un pájaro muy pequeño.
Tiene un nido de musgo y el nido contiene dos huevos pequeños.
¡Qué pequeños deben ser los pajaritos!
María esperaba al colibrí todos los días.
Un día tomó una de las tazas de su muñeca.
Puso un poco de azúcar y agua en la taza.
Después puso la taza en el jardín.
El colibrí voló hacia la taza.
Puso su largo pico en la taza.
Le gustó el agua con azúcar.
¡Qué contenta estaba María!
Tenía azúcar para él todos los días.