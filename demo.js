/* Global variables */
var RULES;

/* Load the weight values right off */
$().ready(function() {
    /* Initialize the rules. This will be filled in by sendMeta("list_rules") */
    RULES = new Object();

    $("#ruletable").on('click', '.remove_rule', function() {

        // TODO: remove from RULES
        // key = $(this).closest('tr').text();
        // alert(key);
        // delete RULES[key];

        $(this).closest('tr').remove();
        translate_default();
    });

    /* Initialize the param string */
    // http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
    $.urlParam = function(name, url) {
        if (!url) {
            url = window.location.href;
        }
        var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(url);
        if (!results) { 
            return undefined;
        }
        return results[1] || undefined;
    }

    /* Set the query string, if specified */
    if ($.urlParam('q')) {
        $("#sourceTxt").val($.urlParam('q'));
    }

    /* Set the port, if specified */
    if ($.urlParam('port')) {
        $("#server_port").val($.urlParam('port'));

        server_connect($("server_host"), $("server_port"));
    }

    /* Loads the text on the main page */
    load_text(1);
    $(document).on('change', 'select', function() {
        var opt = $(this).find('option:selected').text();
        num = opt.split(' ')[1];
        load_text(num);
    });
});

function server_connect(server, port) {
    sendMeta("get_weights");
    sendMeta("list_rules");

    /* Translate the text */
    if ($("#sourceTxt").val())
        translate();
}

function server_changed(e) {
    if (e.which == 13) {
        server = $("#server_host").val()
        port   = $("#server_port").val()

        if (server && port) {
            server_connect(server, port);
            return false;
        }
    }
}

$('#server_host').keypress(server_changed);
$('#server_port').keypress(server_changed);

// Submit the form when pressing enter in the translation box 
$('#sourceTxt').keypress(function (e) {

    if (e.which == 13 && (e.metaKey || e.ctrlKey)) {
        translate_default();

        return false;
    }
});

function gettext() {
    return $("#sourceTxt").val().replace(/([^\.!\?])([\.!\?,]+)/g, "$1 $2");
}

function getserver() {
    return $("#server_host").val()
}

function getport() {
    return $("#server_port").val()
}

/* 
 * Makes sure Joshua isn't going to choke on the phrase
 */
function validate_rule(source, target) {
    if (! source || ! source)
        return false;

    source = " " + source + " ";
    target = " " + target + " ";

    var sourceX = source.indexOf(" X ");
    var targetX = target.indexOf(" X "); 
    var sourceY = source.indexOf(" Y ");
    var targetY = target.indexOf(" Y "); 

    // make sure X is found on both sides or neither
    if ((sourceX == -1 && targetX != -1) || (sourceX != -1 && targetX == -1))
        return false;

    // if X is found, check for Y, too
    if (sourceX != -1)
        if ((sourceY == -1 && targetY != -1) || (sourceY != -1 && targetY == -1))
            return false;

    return true;
}

function substitute_nt(phrase) {
    phrase = " " + phrase.trim() + " ";
    phrase = phrase.replace(" X ", " [X,1] ");
    if (phrase.indexOf("\[X,1\]") != -1)
        phrase = phrase.replace(" Y ", " [X,2] ");
    return phrase.trim();
}

/**
 * Adds a rule to the custom grammar housed in the MT system.
 */
$('#add_rule').click(function() {
    var sourcePhrase = $("#addPhrase_source").val().trim();
    var targetPhrase = $("#addPhrase_target").val().trim();

    if (! validate_rule(sourcePhrase, targetPhrase)) {
        alert("Invalid rule!");
        return;
    }

    sourcePhrase = substitute_nt(sourcePhrase);
    targetPhrase = substitute_nt(targetPhrase);

    var ruleStr = "add_rule [X] ||| " + sourcePhrase + " ||| " + targetPhrase;

    // Add word-word alignment if unambiguous
    // if (sourcePhrase.split().length == 1 && targetPhrase.split().length == 1)
    //     ruleStr += " |||   ||| 0-0";

    sendMeta(ruleStr);

    // clear the values
    $("#addPhrase_source").val("");
    $("#addPhrase_target").val("");

    // display the new rule
    display_rule(sourcePhrase, targetPhrase, "");

    // re-translate
    translate_default();

    return false;
});


// $('#add_weight').click(function() {
//     var html = "<div class=\"input-group\">
//                     <span class=\"input-group-addon\" id=\"label-lm\"></span>
//                     <input type="text" class=\"form-control\" value=\"1.0\" size=\"8\" id=\"lm_weight\"
//                            aria-describedby="label-lm" />
//                   </div>";
//     $("#feature_weights").append(html);
// });


// Change the LM weight
$('.weights').keypress(function (e) {
    if (e.which == 13) {
        /* Build a list of all the weights (items in the "weights" class) */
        var message = "set_weights";
        $(".weights").each(function(i, item) {
            message += " " + item.id + " " + item.value;
        });

        sendMeta(message);
        // sendMeta("get_weights");

        translate();

        return false;
    }
});

// Writes the translation results in the first output box
function record_results(data, status) {
    result = "<ul class=\"list-group\">";

    /* This version outputs the 1-best candidate of multiple input sentences */
    $(data.data.translations).each(function(i, item) {
        // result += item.totalScore + " " + item.hyp + "<br/>\n";
        result += "<li class=\"list-group-item\"><span class=\"badge\">" + (i + 1) + "</span>" + clean_oovs(item.translatedText) + "</li>";
        // result += "<li class=\"list-group-item\">" + clean_oovs(item.translatedText) + "</li>";
    });

    result += "</ul>";
    $("#output").html(result);
};

/**
 * Cleans out OOVs
 */
function clean_oovs(str) {
    str = str.replace(/(\S+)_OOV/g, "<span style='color:red'>$1</span>");
    str = str.replace(/ ([\.\?,])/g, "$1");
    return str;
}
    
// Displays an error message in the output box
function failure(msg) {
    $("#output").html("<div class='alert alert-danger'>" + msg + "</div>");
};

/* 
 * Query the Joshua server. We create a JSON object with the query string
 */
function query_server(text, meta, server, port, success_func) {
    // var url = "http://" + $("#server_host").val() + ":" + $("#server_port").val() + "/";
    var url = "http://" + server + ":" + port + "/";
    var request = $.ajax({
        dataType: 'json',
        url: url,
        data: { "q": text,
                "meta": meta },
        success: success_func,
    });

    request.fail(function(jqXHR, textStatus) {
        failure(textStatus);
    });
};

// Writes the translation results in the output box
function meta_received(data, status) {
    var metadata = data.metaData;
    $(data.metadata).each(function(i, item) {
        tokens = item.split(" ");
        type = tokens[0]

        if (type == "weights") {
            var weights = new Object();
            for (i = 1; i < tokens.length; i++) {
                words = tokens[i].split("=");
                weights[words[0]] = words[1];

                div = $("#" + words[0]);
                if (div.length > 0) {
                    div.val(words[1]);
                }
            }
        } else if (type == "custom_rule") {
            fields = item.substring(item.indexOf(" ") + 1).split(" ||| ");
            lhs = fields[0];
            source = fields[1];
            target = fields[2];
            weights = fields[3];
            alignment = fields[4];

            key = source + " ||| " + target;
            if (! RULES[key]) {
                RULES[key] = weights;
                display_rule(source, target, weights);
            }
        }
    });
}

/* Displays a rule */
function display_rule(source, target, weights) {
    html  = "<tr>";
    html += "<td><button type=\"button\" class=\"btn btn-danger btn-xs remove_rule\" value=\"" + source + " ||| " + target + "\" onclick=\"remove_rule(this)\">X</button></td>";
    html += "<td>" + source + "</td>";
    html += "<td>" + target + "</td>";
    html += "</tr>";
    
    $("#ruletable tbody:last-child").prepend(html);
}

/* Deletes a rule from Joshua, removes from display */
function remove_rule(obj) {
    rule = obj.value;
    tokens = rule.split(" ||| ");
    source = tokens[0];
    target = tokens[1];
    undisplay_rule(source, target);
    sendMeta("remove_rule [X] ||| " + source + " ||| " + target);
}

/* Removes a rule from the display */
function undisplay_rule(source, target) {
    $("#ruletable tbody").each(function(i, item) {
        console.log("looking at " + item);
    });
}

/* Sends metadata to the server */
function sendMeta(metadata) {
    server = $("#server_host").val()
    port   = $("#server_port").val()

    query_server("", metadata, server, port, meta_received);
}

/* Obtains all the parameters from text boxes, then calls the appropriate translate function */
function translate_default() {
    text = gettext();
    server = getserver();
    port = getport();
    translate(text, server, port, record_results);
}

/* 
 * Sends the AJAX query to the server. "fun" is the function that gets called when
 * the asynchronous query comes back.
 */
function translate(text, server, port, fun) {
    query_server(text, "", server, port, fun);
}


/* Loads the text */
function load_text(which) {
    var texts = [
`Jesa Q . vod oxahod !
Jesa Q . giiji qwavob o dwd givjabod .
Sagifab vo jeav pa vod mvisad u vo nibab ab dwd givjabod .
O Joseeo va fwdro jesos vod oxahod .
Va fwdro qasvod sagifas vo jeav .
Bi vo negob .
O avvo va fwdro ouwposvod .
Joseeo gifa wbo xibero mvis ojosevvo u da vo rsoa o wbo oxaho .
Vo oxaho qwavo zogeo vo mvis .
Bi vo nego .
Joseeo ixdasqo vo oxaho sagifeabpi jeav .
Tweasa qas giiji vi zoga .
Aadrod dib oxahod twa zogab jeav .`,
`Wbo oxaho dova pa wb zwaqi .
Nsejasojabra ad wb fwdobi natwabui .
Vod oxahod vi ovejabrob pa nivab .
Sagifab av nivab pa vod mvisad .
Av nivab nosaga nivqi ojosevvi .
Av fwdobi da ovejabro pwsobra gebgi peeod , abribgad nosaga twa qo o pisjes .
Vod oxahod vi gwxsab gib gaso .
Ov nigi reajni da padneasro u dova pa dw gojo pa gaso .
Ad wbo oxaho gzetwerebo .`,
`Joseeo hwfoxo ab av hospeeb wb peeo .
Iuii wb lwjxepi mwasra .
Aso pajodeopi mwasra noso wbo oxaho .
Aso wb givexsee .
Joseeo da twapii twearo noso jesos av givexsee .
Twaa xiberod asob dwd nvwjod !
Twaa onsedo jiqeeo vod overod !
Rabeeo wb negi jwu vosfi .
Nipeeo vvafos gib aav ov mibpi pa vod mvisad .
Av givexsee gija jeav .
Vo sagifa pa vod mvisad .
Ad wb noohosi jwu natwabui .
Reaba wb bepi pa jwdfi u av bepi gibreaba pid zwaqid natwabuid .
Twaa natwabuid paxab das vid nohoserid !
Joseeo adnasoxo ov givexsee ripid vid peeod .
Wb peeo rijii wbo pa vod rolod pa dw jwbuago .
Nwdi wb nigi pa olwwgos u ofwo ab vo rolo .
Padnwaad nwdi vo rolo ab av hospeeb .
Av givexsee qivii zogeo vo rolo .
Nwdi dw vosfi negi ab vo rolo .
Va fwdrii av ofwo gib olwwgos .
Twaa gibrabro adroxo Joseeo !
Rabeeo olwwgos noso aav ripid vid peeod .`,
`Wb soribgeri qeqeeo ab bwadrsod nosapad .
Ripod vod bigzad doveeo noso hwfos .
Qabeeo ov gwosri pa Dimeeo .
Va fwdro hwfos dixsa dw adgserisei .
Dimeeo nibeeo o qagad olwwgos ovvee noso aav .
Av soribgeri vo abgibrsoxo .
Wb peeo jojoo vi qei .
Pehi twa av foreri paxeeo gifasvi .
Dimeeo adroxo jwu rsedra .
Bi twaseeo twa da vi gijeada av foreri .
Zoxvii o nonoo pav soribgeri .
Aav va pei wbo soribaso natwabuero twa nosageeo wbo howvo .
Av soribgeri nipeeo qeqes ab avvo .
Nwdi olwwgos ab vo soribaso .
Av soribgeri abrsii ab vo soribaso .
Avvo va poxo pa gijas ripid vid peeod .
Avvo va poxo ofwo ab vo rolo pa dw jwbuago .
Av soribgeri tweasa o Dimeeo .
Adroo mavel ab dw howvo .`,
`Ui diu wb joueri .
Zofi je bepi ab vid nsopid .
Jeso je zasjidi nvwjoha .
Ad xvobgi u bafsi .
Qa Q . o je gijnobuasero ?
Reaba wb nvwjoha idgwsi .
O jee ja fwdrob vod josfoserod u vod uasxod .
Ja xovobgai ab vid holjebad u ab vod loslod .
Diu rob mavel u rob ovafsa .
Qwavi zogeo vid gojnid pa ossil .
Giji ripi av ossil twa nwapi .
Ui gsai twa av ossil gsaga noso jee .
Gihi ebdagrid , jidgod u fwdobid .
U gsai twa ui nipseeo rabas ossil rojxeaab .
Ab av qasobi qiu ov bisra .
Joueri , joueri ! aadro ad je gobgeiib .
Xwwdgoja ab vid nsopid .`,
`Jesa Q . adro mvis xvobgo .
Doxad twaa mvis ad ?
Ad vo mvis pa vo uwgo .
Bid fwdro qasvo xsevvos ab av div .
Vad fwdro o vod oxahod u o vod nivevvod .
Avvod gifab gijepo pa dwd gonwvvid .
Vo nvobro pa vo uwgo reaba zihod vosfod u nwbreofwpod .
Ad wbo nvobro jwu wwrev .
Gorovebo reaba wbo difo noso dw goxso .
Vo difo adroo zagzo pa vod zihod u pa vid rovvid pa vo uwgo .
Dw jopsa zovvo wwrevad vod soeegad .
Vod ossobgo u vod dago .
Padnwaad vod wdo noso hoxiib .
Voqo av goxavvi pa Gorovebo gib avvod .
Gorovebo reaba wb zasjidi goxavvi bafsi .
Vo uwgo vi niba dwoqa u vwdrsidi .
Av mswri pa vo uwgo ad xwabi noso gijas .
Vo jopsa pa Gorovebo gwaga av mswri .
" Twaa zasjido ad vo uwgo !" pegab vid bebuid .
" Twaa wwrev ad !" pega dw jopsa .
" Bid ovafso o ripid qas vo nvobro pa vo uwgo ."`,
`Wb zwaqi natwabui ad natwabui .
Wb givexsee sagifa jeav pa vo mvis ojosevvo u gija vo jeav .
Joseeo hwfoxo ripid vid peeod ab av hospeeb gib vod mvisad u vod oxahod .
Vo oxaho bi gija pajodeopi jeav .
Vod overod pa vod oxahod asob jwu natwabuid .
Av givexsee natwabui ab av hospeeb rabeeo nvwjod xiberod .
Dw jwbuago bi aso pajodeopi natwabui .
Olwwgos nosaga jeav .
" Jesa Q . av joueri sagifeabpi av ossil !" pega Gorovebo .
Joseeo nwdi gebgi zwaqid u vo mvis ojosevvo ab av bepi natwabui .
Vo oxaho adroxo gibrabro .
Vo uwgo ad wbo nvobro .
Vo mvis ojosevvo bi ad jwu wwrev .
Joseeo gija av mswri nasi bi gija av olwwgos .
Vo jopsa pa Gorovebo voqo dw rolo gib hoxiib zagzi pa vod soeegad pa vo uwgo .`,
`oink`
];
    $("#sourceTxt").text(texts[which-1]);
}
