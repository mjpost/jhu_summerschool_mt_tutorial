It's not too hard to setup. The paths given here are particular to
CLSP. You really do need to install your own version of Joshua,
because each running instance stores its own private rule library, so
we'll have to start up one for each group (either on different servers
or ports).

1. Install dependencies. You'll need

   - Maven >= 3.3.9. If you are on CLSP, this is installed at
     `/home/mpost/code/apache-maven-3.3.9/bin/mvn` (the executable is
     called `mvn`).
     
   - Java 8. Make sure you're using Java 8.
   
         export JAVA_HOME=/usr/lib/jvm/java-8-oracle/

1. Get a copy of Joshua.

       wget -O incubator-joshua-master.zip https://github.com/apache/incubator-joshua/archive/master.zip
       unzip incubator-joshua-master.zip

1. Set some variables, and compile

       cd incubator-joshua-master
       export JOSHUA=$(pwd)
       echo "export JOSHUA=$JOSHUA" >> ~/.bashrc
       mvn package


1. Start Joshua in server mode

       $JOSHUA/bin/joshua -server-type http -server-port 5674 \
         -feature-function OOVPenalty \
         -feature-function "PhrasePenalty -owner custom" \
         -weight-overwrite "OOVPenalty 1 PhrasePenalty -1" \
         -mark-oovs

   Alternately, you can use the config file in this directory, which
   contains all the above parameteres, and simply run it like this:

       $JOSHUA/bin/joshua -config tutorial.config

   Command-line arguments override values in the config file, so if
   you need to change the port only, you can do:
   
       $JOSHUA/bin/joshua -config tutorial.config -server-port 5674
       
1. Load the index file, and make sure the values in the "Parameters"
   tab match your server settings above.
   
That's it!
